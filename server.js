//var MongoClient = require('mongodb').MongoClient;
// create a server
//var server = new http.Server();
//server.listen(process.env.PORT, process.env.IP);
//MongoClient.connect('mongodb://zhur:access@ds053788.mongolab.com:53788/storage', function(err, db) {
//});

var express  = require('express');
var app = express();
var jwt = require('jsonwebtoken');  //https://npmjs.org/package/node-jsonwebtoken

var logger = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var expressValidator = require('express-validator');

var port = process.env.PORT || 8080;

// ------------------------------------------------------------------
mongoose.connect('mongodb://zhur:access@ds053788.mongolab.com:53788/storage');
// ------------------------------------------------------------------


app.use(express.json());
app.use(express.urlencoded());

app.use(express.static(__dirname + '/client')); // set the static files location /public/img will be /img for users
app.use(logger('dev'));     // log every request to the console
app.use(bodyParser());          // pull information from html in POST
app.use(methodOverride());           // simulate DELETE and PUT
app.use(expressValidator());        // required for Express-Validator

app.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.send(401, 'Unauthorized');
  }
});

//REST Api
require('./dispatcher/dispatcher.js')(app);

app.get('/', function(req, res) {
  res.sendfile('./client/templates/index.html');
});

app.listen(port);

console.log("App listening on port ", port);
