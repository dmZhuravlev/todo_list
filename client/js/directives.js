/**
 * Created by DmZhur
 * Directive checkBoxSpan, defaultState, showEdit, blurEdit
 */
angular.module('list-app')
    .directive('checkBoxSpan', function () {
        var checkBoxHandler = function(scope, element, attrs) {
            var helperFindEl = function(arrayVar){
              angular.forEach(arrayVar, function(value, key){
                  if(arrayVar.eq(key).hasClass('todo-checkbox')){
                    arrayVar.eq(key).prop('checked', !arrayVar.eq(key).prop('checked'));
                  }
                });
            };

            element.bind('click', function() {
              var arrayEl = element.parent();
              var idItem = arrayEl.find('input').eq(1).attr('attr-id');
              var checkedFlag = arrayEl.find('input')[0].checked;

              scope.checkEditTodo(idItem, !checkedFlag);

              scope.flagChecked = scope.flagChecked ? false : true;

              helperFindEl(arrayEl.find('input'));
            });
        };
        return {
            restrict: 'A',
            link: checkBoxHandler
          }
    });

angular.module('list-app')
    .directive('defaultState',function(){
      var setProperty = function(scope, element, attrs){
        attrs.defaultState === 'true' && element.eq(0).prop('checked', true);
      };
      return {
          restrict: 'A',
          link: setProperty
        }
    });

angular.module('list-app')
    .directive('showEdit', function() {
    var funcEditShow = function(scope, element, attrs) {

      element.bind('click', function() {
        var editElem = element.next();
        element.addClass('hide-edit');
        editElem.addClass('show-edit');
        editElem[0].focus();
      });

      scope.$watch(attrs.showEdit, function() {
        element.removeClass('hide-edit');
      });
    };
    return {
      restrict: 'A',
      link: funcEditShow
    }
  });

angular.module('list-app')
    .directive('blurEdit', function () {
    var funcBlurEdit = function(scope, element) {

      scope.flagOfBlur = false;
      element.bind('blur', function () {

        scope.editTodo(element.attr('attr-id'), element.val());

        element.parent().find('label').text(element.val());
        element.removeClass('show-edit');

        if(!scope.flagOfBlur) {
          scope.flagOfBlur = true;
        } else {
          scope.flagOfBlur = false;
        }
        // when the flagOfBlur changed, this change starting $apply
        //scope.$apply(attrs.blurEdit);
      });
    };
    return {
      restrict: 'A',
      link: funcBlurEdit
    }
  });
