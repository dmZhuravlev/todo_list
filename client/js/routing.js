
//Now we have the JWT saved on sessionStorage.
//If the token is set, we are going to set the Authorization header for every outgoing request done using $http.
//As value part of that header we are going to use Bearer <token>.

angular.module('list-app', ['ngRoute'])
  .factory('authInterceptor', function ($rootScope, $q, $window) {
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if ($window.sessionStorage.token) {
          config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
        }
        return config;
      },
      responseError: function (rejection) {
        if (rejection.status === 401) {
          // handle the case where the user is not authenticated
        }
        return $q.reject(rejection);
      }
    };
  });

angular.module('list-app')
    .config(function ($httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
    });

// ~~~~~~~~~~~~~~~  ROUTING ~~~~~~~~~~~~~~~~~~~~~~
angular.module('list-app')
  .config(function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: 'templates/login.html'
        //controller: 'UserCtrl',
      }).
      when('/list', {
        templateUrl: 'templates/list_items.html',
        access: { requiredAuthentication: true }
        //controller: 'MainContainer'
      }).
      when('/registration', {
        templateUrl: 'templates/registration.html'
      }).
      otherwise({
        redirectTo: '/login'
      });
  });
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
angular.module('list-app')
  .run(function($rootScope, $location, $window) {
    $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
        //redirect only if both isAuthenticated is false and no token is set
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
            && !$window.sessionStorage.token) {
            $location.path('/login');
        }
    });
  });