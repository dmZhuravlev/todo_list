/**
 * Created by DmZhur
 */
angular.module('list-app')
    .controller('MainContainer', function ($scope, $http, $window, $location) {
        $scope.formData = {};

        // ----- receive database ($scope.todos) from mongodb --------
        $http.get('/api/todos')
          .success(function(data) {
            $scope.todos = data;
          })
          .error(function(data) {
            //delete $window.sessionStorage.token;
            if(data.err === 'Access token has expired'){
              delete $window.sessionStorage.token;
              $location.path('/login');
            }
            $scope.todos = [{}];
          });

        // when submitting the add form, send the text to the node API
        $scope.addingToList = function() {
          $http.post('/api/todos', $scope.formData)
            .success(function(data) {
              // clear the form so our user is ready to enter another
              $scope.formData = {};
              $scope.todos = data;
            })
            .error(function(data) {
              console.log('Error: ' + data);
            });
        };

        // remove item
        $scope.deleteTodo = function(id_todo) {
          $http.post('/api/remove-item', {'id_el': id_todo})
            .success(function(data) {
              $scope.todos = data;
            })
            .error(function(data) {
              console.log('Error: ' + data);
            });
        };

        // edit item
        $scope.editTodo = function(id_todo, item_val) {
          $http.post('/api/edit-item',{'id_el': id_todo, 'item_text': item_val})
          .success(function (data) {
            console.log('edit complete:' + data);
          })
          .error(function(data){
            console.log('Error: ' + data);
          });
        };

        // edit item checked unchecked
        $scope.checkEditTodo = function(id_todo, boolean_prop){
          $http.post('/api/edit-checked',{'id_el': id_todo, 'item_flag': boolean_prop})
          .success(function (data) {
            console.log('edit complete:' + data);
          })
          .error(function(data){
            console.log('Error: ' + data);
          });
        };

        //logout
        $scope.logout = function(){
            delete $window.sessionStorage.token;
            $location.path('/login');
        }

        //archive function
        // $scope.archiveItem = function() {
        //   var oldList = $scope.databservice.listOfItem;

        //   $scope.databservice.listOfItem = [];
        //   angular.forEach(oldList, function(key) {
        //     if(!key['done']) {
        //       $scope.databservice.listOfItem.push(key);
        //     }
        //   });
        // };
    });

angular.module('list-app')
    .controller('UserCtrl', function ($scope, $http, $window, $location) {

        if ($window.sessionStorage.token) {
          $scope.isAuthenticated = true;
        } else {
          $scope.isAuthenticated = false;
        }

        $scope.submit = function () {
          $scope.user = {
            username: $scope.valName,
            password: $scope.valPass
          };

          $http
            .post('/authenticate', $scope.user)
            .success(function (data) {
              $window.sessionStorage.token = data.token;
              $scope.isAuthenticated = true;

              // !!!!
              $location.path('/list');
            })
            .error(function (data) {
              // Erase the token if the user fails to log in
              delete $window.sessionStorage.token;

              // Handle login errors here
              $scope.error = 'Error: Invalid user or password';
            });
        };

        $scope.logout = function () {
          $scope.isAuthenticated = false;
          delete $window.sessionStorage.token;
        };

        $scope.goRegForm = function () {
            $location.path('/registration');
        };
    });

angular.module('list-app')
    .controller('RegistrationUser', function ($scope, $http, $window, $location) {
    $scope.submit = function () {
        $scope.user = {
            name: $scope.valueNm,
            password: $scope.valuePass
        };

        $http.post('/registration', $scope.user)
            .success(function (data) {
                $window.sessionStorage.token = data.token;
                $scope.isAuthenticated = true;

                // !!!!
                $location.path('/list');
            })
            .error(function (data) {
                $scope.error = 'Error:'+ data;
            });
    };
});