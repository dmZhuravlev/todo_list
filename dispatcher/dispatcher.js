/**
 * Created by DmZhuravlev
 *
 */

module.exports = function(app) {
var user = require('../model/model.js');
var jwt = require('jsonwebtoken');

/*
 * SECRET KEY (We are going to protect /api routes with JWT)
 */
var secret = 'this is the secret secret secret 12356';

var checkAuth = function (req) {
    if (!req.headers || !req.headers.authorization) {
        return {err: 'header authorization'};
    }
    var parts = req.headers.authorization.split(' ');

    if (parts.length != 2) {
        return {err: 'Format is Authorization: Bearer [token]'};
    }

    var scheme = parts[0];
    var credentials = parts[1];

    if (/^Bearer$/i.test(scheme)) {
        var token = jwt.decode(credentials);
        var date = new Date;

        date = Math.floor(date.getTime()/1000);
        console.log('token session',token.exp, date);

        if(token.exp <= date){
            return {err: 'Access token has expired'};
        } else {
            return token;
        }
    } else {
        return {err: 'Format is Authorization: Bearer [token]'};
    }

};
/*
 * Authenticate
 */
    app.post('/authenticate', function (req, res) {
      user.find({ 'name': req.body.username,
                   'password': req.body.password
                }, function (err, item) {
          if (err) return res.send("contact addMsg error: " + err);

          if(!item.length) {
            //if is invalid, return 401
            res.send(401, 'Wrong user or password');
            return;
          }
          var profile = {
            idUser: item[0]._id
          };

          // We are sending the profile inside the token
          var token = jwt.sign(profile, secret, { expiresInMinutes: 30 });
          res.json({ token: token });
      });
    });

/*
 * Send back all items
 */
    app.get('/api/todos', function(req, res) {
        var token = {};
        var checkResult = checkAuth(req);

        if (checkResult['err']) {
            return res.json(401, checkResult);
        } else {
            token = checkResult;

            user.findOne({
                '_id': token['idUser']}, function (err, item) {
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                err && res.send(err);
                // return all todos in JSON format
                if (!item.itemList) {
                    item.itemList = {};
                }
                res.json(item.itemList);
            });
        }

    });

/**
 * Adding item
 */
    app.post('/api/todos', function(req, res) {
        var token = {};
        var checkResult = checkAuth(req);

        if (checkResult['err']) {
            return res.json(401, checkResult);
        } else {
            token = checkResult;

            user.update({_id: token['idUser']},
            {
                $push: {'itemList': {
                        done: false,
                        text: req.body.text
                    }
                }
            }, function(err, numAffected, rawResponse) {
                if (err) {
                    return res.send("contact addMsg error: " + err);
                }
                user.findOne({
                    '_id': token['idUser']}, function (err, item) {
                    err && res.send(err);
                    res.json(item.itemList);
                });
            });
        }
    });

/*
 * Delete a item
 */
    app.post('/api/remove-item', function(req, res) {
        var token = {};
        var checkResult = checkAuth(req);

        if (checkResult['err']) {
            return res.json(401, checkResult);
        } else {
            token = checkResult;

            user.update({_id: token['idUser']},
            {
                $pull: {'itemList': {
                    _id : req.body.id_el
                  }
            }
            }, function(err, numAffected, rawResponse) {
                if (err) {
                    return res.send("contact addMsg error: " + err);
                }
                user.findOne({
                    '_id': token['idUser']}, function (err, item) {
                    err && res.send(err);
                    res.json(item.itemList);
                });
            });
        }

    });

/*
 * Edit item
 */
    app.post('/api/edit-item', function(req, res) {
        var token = {};
        var checkResult = checkAuth(req);

        if (checkResult['err']) {
            return res.json(401, checkResult);
        } else {
            token = checkResult;

            user.update({'_id': token['idUser'], 'itemList._id': req.body.id_el},
            {
                $set:{
                  'itemList.$.text': req.body.item_text
                }
            }, function(err, numAffected, rawResponse) {
                if (err) {
                    return res.send("contact addMsg error: " + err);
                }
                user.findOne({
                    '_id': token['idUser']}, function (err, item) {
                    err && res.send(err);
                    res.json(item.itemList);
                });
            });
        }
    });
    /*
     * Edit item check or un_check
     */
    app.post('/api/edit-checked', function(req, res) {
        var token = {};
        var checkResult = checkAuth(req);

        if (checkResult['err']) {
            return res.json(401, checkResult);
        } else {
            token = checkResult;

            user.update({'_id': token['idUser'], 'itemList._id': req.body.id_el},
            {
                $set:{
                  'itemList.$.done': req.body.item_flag
                }
            }, function(err, numAffected, rawResponse) {
                if (err) {
                    return res.send("contact addMsg error: " + err);
                }
                user.findOne({
                    '_id': token['idUser']}, function (err, item) {
                    err && res.send(err);
                    res.json(item.itemList);
                });
            });
        }
    });
    /*
    * Registration new user
    */
    app.post('/registration', function (req, res) {
        var errors = null;
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('password', 'Password is required').notEmpty();

        errors = req.validationErrors();

        if (!errors) {
            user.find({
                'name': req.body.name
            }, function (err, item) {
                if (err) return res.send("contact addMsg error: " + err);
                console.log('test', item);
                if (!item.length) {
                    var userElem = new user({
                        name: req.body.name,
                        password: req.body.password,
                        itemList : []
                    });
                    var setToken = function (itemEl) {
                        var profile = {
                            idUser: itemEl._id
                        };

                        // We are sending the profile inside the token
                        var token = jwt.sign(profile, secret, { expiresInMinutes: 30 });
                        res.json({ token: token });
                    };

                    userElem.save(function (err, data) {
                        if (err) {
                            return res.send(401, 'Error' + err);
                        } else {
                            console.log('Saved ', data);
                            user.findOne({
                                'name': data.name
                            }, function (err, item) {
                                if (err) return res.send("contact addMsg error: " + err);
                                item && setToken(item);
                            });
                        }
                    });
                } else {
                    return res.send(401, "This name is already exist");
                }

            });
        } else {
            console.log('errors', errors);
            return res.send(401, ' Error' + errors[0].msg);
        }
    });
};