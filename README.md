# TODO List App

Application for self improvement powered by DmZhuravlev.
### Version 
0.0.1
### Tech
  - AngularJS
  - NodeJS
  - Mongolab (MongoDB Hosting)

### Instaletion
First of all, clone a repository from bitbucket
```sh
 git clone git@bitbucket.org:dmZhuravlev/todo_list.git
```
You need install NodeJS, on your local machine.
After that  you need install all modules from package.json.
For that you need go to folder with project, and type command bellow

```sh
 npm install
```

Also you need install bower (package manager for the web)

```sh
npm install -g bower
```

And install all packages from bower.json

```sh
bower install
```

We almost finished. Now we need run server. For that use command bellow

```sh
node server.js
```

That's it, open your browser and type http://127.0.0.1:8080/

For authorisation you can use this user
> name: test_new_user

> password: qwerty

Or you can "Sign up"
