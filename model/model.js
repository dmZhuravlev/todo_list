/**
 * define model
 *
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
    name: {
      type: String,
      unique: true
    },
    password: String,
    itemList: [{
      done: Boolean,
      text: String
    }]
}, {collection: 'users'});

var User = mongoose.model('User', userSchema);
module.exports = User;

//example of adding some user
//    //mongoose.set('debug', true);
//
//    var userElem = new User({
//    name: 'test_Name111',
//    password: 'test_password_22',
//    itemList : [{
//        done: true,
//        text: "test5"
//    },{
//        done: true,
//        text: "test11"
//    },{
//        done: true,
//        text: "www_test5"
//    }]
//    });
//
//    userElem.save(function (err, data) {
//        if (err) {
//            console.log(err);
//        } else {
//            console.log('Saved ', data);
//        }
//    });
